import StocksModel from "../es6/Stocks.model.js";
import Midprices from "../es6/midprices.js";

describe("StocksModel", () => {

    test("set() saves stockData and get() returns sorted array", () => {
        const stock = new StocksModel(),
        data = {
            "name": "Etherium",
            "amount": 700,
            "bestBid": 33.33,
            "bestAsk": 22.22,
            "openAsk": 44.33,
            "openBid": 55.22,
            "lastChangeAsk": 0.55448888,
            "lastChangeBid": 0.99448888
        };
        stock.set(data);
        expect(stock.get()).toEqual([data]);
    });

    test("set() creates a new Stock and midprices", () => {
        const stock = new StocksModel(),
        data = {
            "name": "Etherium",
            "amount": 700,
            "bestBid": 33.33,
            "bestAsk": 22.22,
            "openAsk": 44.33,
            "openBid": 55.22,
            "lastChangeAsk": 0.55449999,
            "lastChangeBid": 0.99448888
        };
        stock.set(data);
        expect(stock.get()[0].midprices).toEqual({ _midprices: [ 27.775 ], _time: 30000 });
    });

    test("set() updates existing Stock and midprices", () => {
        const stock = new StocksModel(),
        data1 = {
            "name": "Etherium",
            "amount": 700,
            "bestBid": 33.33,
            "bestAsk": 22.22,
            "openAsk": 44.33,
            "openBid": 55.22,
            "lastChangeAsk": 0.55449999,
            "lastChangeBid": 0.99448888
        },
        data2 = {
            "name": "Etherium",
            "amount": 700,
            "bestBid": 13.33,
            "bestAsk": 12.22,
            "openAsk": 34.33,
            "openBid": 55.22,
            "lastChangeAsk": 9.99445555,
            "lastChangeBid": 7.99445555
        };

        stock.set(data1);
        stock.set(data2);

        expect(stock.get().length).toBe(1);
        expect(stock.get()[0].midprices.get()).toEqual([ 27.775, 12.775 ]);
    });


    test("get() returns sorted data (defaul sort by lastChangeBid)", () => {
        const stocks = new StocksModel();
        const best = {
            "name": "Etherium",
            "amount": 700,
            "bestBid": 33.33,
            "bestAsk": 22.22,
            "openAsk": 44.33,
            "openBid": 55.22,
            "lastChangeAsk": 9.99445555,
            "lastChangeBid": 7.99445555
        };
        const normal = {
            "name": "Litecoin",
            "amount": 500,
            "bestBid": 11.33,
            "bestAsk": 11.22,
            "openAsk": 44.33,
            "openBid": 55.22,
            "lastChangeAsk": 5.55333333,
            "lastChangeBid": 4.33334444
        };
        const lowest = {
            "name": "Ripple",
            "amount": 100,
            "bestBid": 11.33,
            "bestAsk": 11.22,
            "openAsk": 4.33,
            "openBid": 5.22,
            "lastChangeAsk": 0.55333333,
            "lastChangeBid": -4.010104444
        };

        // lastChangeAsk decides which one is highest
    
        stocks.set(lowest);
        stocks.set(best);
        expect(stocks.get().map(item => item.name)).toEqual(["Etherium", "Ripple"]);

        stocks.set(normal);
        expect(stocks.get().map(item => item.name)).toEqual(["Etherium", "Litecoin", "Ripple"]);
    });

});


describe("Midprices", () => {

  test("Creates a new empty Midprices object", () => {
    expect((new Midprices()).get()).toEqual([]);
  })

  test("Creates a new Midprices object with a value", () => {
    const midprices = new Midprices({ bestBid: 6, bestAsk: 4 });
    expect(midprices.get()).toEqual([5]);
  })

  test("update() returns THIS, the midprices object", () => {
      const midprices = new Midprices();
      // midprices.update returns THIS
      expect(midprices).toEqual(midprices.update({ bestBid: 1, bestBid: 2 }));
  })

  test("update() adds calculated averages to the midprices array, in order received", () => {
      const midprices = new Midprices()
      const stocks = [{
          bestAsk: 10,
          bestBid: 20
      },
      { 
          bestAsk: 3,
          bestBid: 3
      },
      {
          bestAsk: 3,
          bestBid: 1
      }];
      stocks.forEach(item => midprices.update(item));
      expect(midprices.get()).toEqual([15, 3, 2]);
  });
});