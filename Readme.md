Stocks APP Test at EE
===============================
# SCREENSHOT
![DEMO-GIF](https://media.giphy.com/media/xT0xesMJEj7Dir7BsI/giphy.gif)


# Overview
The app is made up of 4 JS files;
 - App.js
 - midprices.js
 - Stocks.controller.js
 - Stocks.model.js

App runs at index.js `app.start({ divId: "root" })` while App Template is picked from `index.html`

The `Stomp` client initilizes inside App.js and Stomp data is passed via `connectCallback` everytime its received.

Upon receipt data is modeled inside `Stocks.model.js` and sorted inside a mapped array, which eventually calls of `Mideprices.update` to create Sparkline array of last 30 seconds of received prices.

The model is sent to `StocksController.updateStockRows` which takes care of generating the DOM and replacing everytime data changes.




To view them, run

```
npm install
npm start
```


# TEST
```
npm run test
```
