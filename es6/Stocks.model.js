import Midprices from "./midprices.js"

class StocksModel {
    constructor ({ sortColumn = "lastChangeBid" } = {}) {
        this._store = [];
        this._sortColumn = sortColumn;
    }

    get() {
        // Returns sorted array by sortColumn
        return this._store.sort((a, b) => b[this._sortColumn] - a[this._sortColumn])
    }

    set(stockData) {
        let index = this._store.findIndex(i => i.name === stockData.name);
        if (index !== -1) {
            this._store[index] = this._updateMidprices(stockData, this._store[index]);
        } else {
            this._store.push(this._createMidprices(stockData));
        }
    }

    _createMidprices(stockData) {
        return Object.assign(stockData, { midprices: new Midprices(stockData) })
    }

    _updateMidprices(newData, existingStock) {
        return Object.assign(newData, { midprices: existingStock.midprices.update(newData)} );
    }
}

module.exports = StocksModel;
