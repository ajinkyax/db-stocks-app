const { Stomp } = require("../site/stomp.js");
const StocksController = require('./Stocks.controller.js');
const StocksModel = require('./Stocks.model.js');

const STOMP_URL = "ws://localhost:8011/stomp";
const client = Stomp.client(STOMP_URL);

function start({divId}) {
    const columns = [
        "name",
        "bestBid",
        "bestAsk",
        "openBid",
        "openAsk",
        "lastChangeBid",
        "lastChangeAsk",
        "midprices"
    ];
    const StocksData = new StocksModel({sortColumn: "lastChangeBid"});
    const StocksCtrl = new StocksController({ divId, columns });

    client.connect({}, function connectCallback() {
        client.subscribe("/fx/prices", function clientConnected(response){
            const body = JSON.parse(response.body);
            StocksData.set(body);
            StocksCtrl.updateStockRows(StocksData.get());
        });
    }, errorHandler);

    function errorHandler (error) {
        console.error('Stomp client error', error);
        alert(error);
    }
}

export { start }