const DEFAULT_TIME = 30; // in seconds

/**
 * Mideprices returns and array of numbers
 * retuns Averages of {bestBid, bestAsk}
 */

class Midprices {
    static average (a, b) {
        return (a + b) / 2;
    }

    constructor (stock, { time = DEFAULT_TIME } = {}) {
        this._midprices = [];
        this._time = time * 1000; // time in ms
        if (stock) {
            this.update(stock);
        }
    }

    update({ bestBid, bestAsk }) {
        this._midprices.push(Midprices.average(bestBid, bestAsk));
        setTimeout(() => this._midprices.shift(), this._time);
        return this;
    }

    get() {
        return this._midprices
    }
}

module.exports = Midprices;