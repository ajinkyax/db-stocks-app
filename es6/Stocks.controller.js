import Sparkline from "../site/sparkline.js"

/*
* A class to control the stock prices DOM.
*/
class StocksController {
    constructor ({ divId, columns, rows = 11, decimal = 5 } = {}) {
        const $div = document.getElementById(divId);
        if (!$div || !columns) {
            throw new Error("Columns or DIV element not found");
        }
        this.DECIMAL = decimal;
        this.TOTAL_ROWS = rows;
        this.COLUMNS = columns;
        this._$div = $div
        this._initializeDOM();
    }

    _initializeDOM () {
        const [row] = this._$div.getElementsByClassName("stock-row")
        Array(this.TOTAL_ROWS)// loop n times
            .fill()
            .forEach(() => {row.insertAdjacentElement("afterend", row.cloneNode(true))}) // true -> clone deep
    }

    /*
    * Populates the "stock-row" with values. Data must be an array of objects
    */
    updateStockRows (data) {
        const stockRows = this._$div.getElementsByClassName("stock-row")
        data.forEach((values, i) => {
            if (i >= stockRows.length) {
                throw new Error("Number of data elements exceeds number of stock rows")
            }
            this._setStockId(stockRows[i], values);
            this._updateRowValues(stockRows[i], values);
        });
    }

    _setStockId(stockRow, values) {
        stockRow.setAttribute('id', `stock-id-${values.name}`);
    }

    _updateRowValues (row, values) {
        const cells = row.getElementsByClassName("cell")
        this.COLUMNS.forEach((col, i) => {
            this._setCellContent(cells[i], values[col], col)
        })
    }

    _setCellContent (cell, value, col) {
        if (col === "name") {
            cell.innerHTML = (value.slice(0, 3) + '-' + value.slice(3)).toUpperCase();// USD-EUR
        } else if (col === "midprices") {
            this._updateSparkline(cell, value);
        } else {
            cell.innerHTML = parseFloat(value).toFixed(this.DECIMAL);
        }
    }

    _updateSparkline (cell, midprices) {
        Sparkline.draw(cell, midprices.get());
    }
}

module.exports = StocksController;